block=$1
limit=$2
while [ $block -le $((limit-1000+1)) ]
do
    rows=$(curl -s 'https://api.thegraph.com/subgraphs/name/blocklytics/ethereum-blocks' -X POST --data-raw '{"query":"{blocks(first:1000 orderBy:number orderDirection:asc where: {number_gte:'$block'}){number timestamp gasUsed gasLimit size}}"}' | jq -r '.data.blocks | map([.number, .timestamp, .gasUsed, .gasLimit, .size] | join(",")) | join("\n")')
    readarray <<<"$rows" rows
    if [ ${#rows[@]} == 1000 ]
    then
        printf "%s" "${rows[@]}"
        block=$((block+1000))
    else
        echo $res > /dev/stderr
    fi
    sleep 1.1
done
