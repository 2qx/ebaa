#!/bin/bash

# output file
outfile=$1

# dataset to process
infile=$2

# algo args
excessiveblocksize=$3
n0=$4
alpha0_xB7_slow=$5
windowLength_slow=$6
alpha0_xB7_fast=$7
windowLength_fast=$8

tmpsim=$(mktemp)

awk -F ',' '{OFS=",";print $3}' "$infile" \
| ../../implementation-c/bin/abla-dual-median -excessiveblocksize $excessiveblocksize -ablaconfig $n0,$alpha0_xB7_slow,$windowLength_slow,$alpha0_xB7_fast,$windowLength_fast \
> $tmpsim

tmpact=$(mktemp)
tmpeb=$(mktemp)
tmpcf=$(mktemp)

awk -F ',' '{OFS=",";print $1,$3,$3}' "$infile" | ../../implementation-c/bin/aggregate -windowlength 144 | awk -F ',' '{OFS=",";print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10}' > $tmpact
awk -F ',' '{OFS=",";print $1,$2,$3}' $tmpsim | ../../implementation-c/bin/aggregate -windowlength 144 | awk -F ',' '{OFS=",";print $1,$11,$12,$13,$14}' > $tmpeb
awk -F ',' '{OFS=",";print $1,$2,(($4>$5)?$4:$5)}' $tmpsim | ../../implementation-c/bin/aggregate -windowlength 144 | awk -F ',' '{OFS=",";print $1,$11,$12}' > $tmpcf
sed -i '1 s/openExcessiveBlockSize/blocksizeLimit/' $tmpeb
sed -i '1 s/closeExcessiveBlockSize/blocksizeLimit/' $tmpeb
sed -i '1 s/openExcessiveBlockSize/dualMedianBlockSize/' $tmpcf
sed -i '1 s/closeExcessiveBlockSize/dualMedianBlockSize/' $tmpcf
join --header  --nocheck-order -t, -1 1 -2 1 $tmpact $tmpeb >$tmpsim
join --header  --nocheck-order -t, -1 1 -2 1 $tmpsim $tmpcf >$outfile

rm $tmpact
rm $tmpsim
rm $tmpeb
rm $tmpcf
