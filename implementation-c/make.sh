mkdir -p bin
gcc -Wall -Wextra src/aggregate.c -lm -o bin/aggregate
gcc -Wall -Wextra src/abla-median.c -o bin/abla-median
gcc -Wall -Wextra src/abla-dual-median.c -o bin/abla-dual-median
gcc -Wall -Wextra src/abla-ewma-elastic-buffer.c -o bin/abla-ewma-elastic-buffer
gcc -Wall -Wextra src/abla-ewma-elastic-buffer-bounded.c -o bin/abla-ewma-elastic-buffer-bounded
